package consolegui

import connQak.connQakBase
import it.unibo.`is`.interfaces.IObserver
import java.util.Observable
import connQak.ConnectionType
import it.unibo.kactor.MsgUtil

class consoleGuiSimple( val connType : ConnectionType, val hostIP : String,   val port : String,
						val ctxDest : String,  val destName : String ) : IObserver {
// smartbell
lateinit var connQakSupport : connQakBase
// waiter
lateinit var connQakSupportWaiter : connQakBase	
	
  		 //val buttonLabels = arrayOf("e","w", "s", "l", "r", "z", "x", "b", "p", "h")
  		 //val buttonLabels = arrayOf("alarm","start","p","h")
	val buttonLabels = arrayOf( "ring", "wantToOrder", "order", "finished", "pay" )
	
	init{
		create( connType )
	}
		 fun create( connType : ConnectionType){
			 // smartbell
			 connQakSupport = connQakBase.create(connType, hostIP, "8010","ctxtearoom", "smartbell" )
			 connQakSupport.createConnection()
			 // waiter
			 connQakSupportWaiter = connQakBase.create(connType, hostIP, "8060","ctxwaiter", "waiter" )
			 connQakSupportWaiter.createConnection()
			 
			 var guiName = ""
			 when( connType ){
				 ConnectionType.COAP -> guiName="GUI-COAP"
				 ConnectionType.MQTT -> guiName="GUI-MQTT"
				 ConnectionType.TCP  -> guiName="GUI-TCP"
 				 ConnectionType.HTTP -> guiName="GUI-HTTP"
			 }
			 createTheGui( guiName )
		  }
		  fun createTheGui( guiName : String ){
	  			val concreteButton = ButtonAsGui.createButtons( guiName, buttonLabels )
	            concreteButton.addObserver( this )
		  }
	 
	
	  
	  override fun update(o: Observable, arg: Any) {
    	  var moveString = arg as String
		  
		  val clientNumber :Int = moveString.substring(moveString.length - 1, moveString.length).toInt()
		  println("consoleGuiSimple update clientNumber=" + clientNumber )
		  
		  val selectedTable : Int = ButtonAsGui.tableCombos[clientNumber - 1].getSelectedItem().toString().toInt()
		  val selectedId : Int = ButtonAsGui.idCombos[clientNumber - 1].getSelectedItem().toString().toInt()
		  val selectedProduct = ButtonAsGui.productCombos[clientNumber - 1].getSelectedItem().toString()
		  val productFee = ButtonAsGui.menuFees[ButtonAsGui.productCombos[clientNumber - 1].getSelectedIndex() + 1]
		  
		  ButtonAsGui.lastMessageId = selectedId
		  ButtonAsGui.simulatorMessageSender = clientNumber - 1
		  
		  println("consoleGuiSimple update selectedId=" + selectedId + " ; selectedTable=" + selectedTable + " ; selectedProduct=" + selectedProduct + " ; fee=" + productFee);
		  
		  val move = moveString.substring(0, moveString.length - 1)
		  println("consoleGuiSimple update arg=" + move )
		  if( move == "ring"){
			  val msg = MsgUtil.buildRequest("clientsimulator", "notifyEntrance", "notifyEntrance(0)", "smartbell" )
			  connQakSupport.request( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString())
		  } 
		  else if( move == "wantToOrder" ){
			  val msg = MsgUtil.buildDispatch("clientsimulator","readyForOrder","readyForOrder( $selectedId, $selectedTable )", "waiter" )
			  connQakSupportWaiter.forward( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString())
			  ButtonAsGui.enableButton(ButtonAsGui.simulatorMessageSender, 2)
		  } else if( move == "order" ){
			  val msg = MsgUtil.buildReply("clientsimulator", "clientOrder", "clientOrder( $selectedProduct, $selectedId, $selectedTable )", "waiter");
			  //val msg = MsgUtil.buildDispatch("clientsimulator", "clientOrder", "clientOrder( caffe, 1, 1 )", "waiter");
			  connQakSupportWaiter.emit( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString())
			  ButtonAsGui.enableButton(ButtonAsGui.simulatorMessageSender, 3)
		  } else if( move == "finished" ){
			  val msg = MsgUtil.buildDispatch("clientsimulator","consumeFinished","consumeFinished( $selectedId, $selectedTable )", "waiter" )
			  connQakSupportWaiter.forward( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString())
			  ButtonAsGui.enableButton(ButtonAsGui.simulatorMessageSender, 4)
		  } else if( move == "pay" ){
			  val msg = MsgUtil.buildReply("clientsimulator", "payment", "payment( $productFee )", destName);
			  connQakSupportWaiter.forward( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString())
			  ButtonAsGui.clearTempLabel(clientNumber - 1)
			  ButtonAsGui.enableButton(ButtonAsGui.simulatorMessageSender, 0)
		  } else{
			  val msg = MsgUtil.buildDispatch("console", "cmd", "cmd($move)", destName )
			  connQakSupport.forward( msg )
			  ButtonAsGui.msgLabel.setText(msg.toString());
		  }
       }//update
}


fun main(){
	consoleGuiSimple( ConnectionType.COAP, hostAddr, port, ctxqadest, qakdestination)
}