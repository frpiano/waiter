package consolegui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import android.text.Layout.Alignment;

import javax.swing.JComboBox;

public class ButtonAsGui extends Observable implements  ActionListener{
	
	static int TABLE_NUMBER = 2;
	public static JComboBox[] tableCombos = new JComboBox[TABLE_NUMBER];
	public static JComboBox[] idCombos = new JComboBox[TABLE_NUMBER];
	public static JComboBox[] productCombos = new JComboBox[TABLE_NUMBER];
	public static Label msgLabel;
	public static Label[] tempLabel = new Label[TABLE_NUMBER];
	
	public static String[] menuProducts = { "Tea", "Caffe", "Coke", "GinTonic" };
	public static int[] menuFees = { 0, 2, 2, 3, 7, 10 };
	
	public static int lastMessageId = 0;
	public static int simulatorMessageSender = 0;
	
	private static ButtonBasic[][] buttons = new ButtonBasic[TABLE_NUMBER][5];

	//Factory method
	public static ButtonAsGui createButtons(  String logo, String[] cmd  ){
		Frame fr = Utils.initFrame(600, 400);
		fr.add( new Label( logo ), BorderLayout.NORTH );
	 	//Panel p = new Panel();
	 	//JPanel p = new JPanel();
	 	JPanel[] p = new JPanel[TABLE_NUMBER];
	 	
	 	String[] intValues = new String[20];
	 	String[] tableValues = new String[TABLE_NUMBER];
	 	
	 	
	 	Font defaultFont = new Font(fr.getFont().getName(), Font.BOLD, 14);
	 	Color backgroundColor = Color.WHITE;
	 	Color labels_backgroundColor = Color.GREEN;
	 	
	 	for(int i = 0; i < intValues.length; i++) {
	 		intValues[i] = ""+ (i + 1);
	 		if(i < tableValues.length)
	 			tableValues[i] = intValues[i];
	 	}
	 	
	 	for(int i = 0; i < p.length; i++) {
	 		p[i] = new JPanel();
	 		p[i].setBorder(BorderFactory.createTitledBorder("Client Simulator " + (i + 1)));
	 	 	p[i].setLayout(new GridLayout(2,3));
	 	 	p[i].setBackground(backgroundColor);
	 	}
	 	
		ButtonAsGui button = new ButtonAsGui();
		for(int k = 0; k < p.length; k++) {
			
			for( int i = 0; i < cmd.length; i++) {
				ButtonBasic b = new ButtonBasic(p[k], cmd[i] + (k + 1), button);	//button is the listener
				buttons[k][i] = b;
				if(i > 0)
					b.setEnabled(false);
				b.setBackground(labels_backgroundColor);
				if(i == 2) {
					JPanel pan = new JPanel();
					Label label = new Label("Product:");
					label.setAlignment(Label.CENTER);
					label.setFont(defaultFont);
					JComboBox combo = new JComboBox(menuProducts);
					combo.setFont(defaultFont);
					productCombos[k] = combo;
					
					combo.setSelectedIndex(k);
					pan.add(label);
					pan.add(combo);
					pan.setLayout(new GridLayout(1,2));
					pan.setBackground(labels_backgroundColor);
					p[k].add(pan);
				}
			}
			JPanel tablePanel = new JPanel();
			Label tableLabel = new Label("Table:");
			tableLabel.setAlignment(Label.CENTER);
			tableLabel.setFont(defaultFont);
			JComboBox tableCombo = new JComboBox(tableValues);
			tableCombo.setFont(defaultFont);
			tableCombos[k] = tableCombo;
			
			tableCombo.setSelectedIndex(k);
			tablePanel.add(tableLabel);
			tablePanel.add(tableCombo);
			tablePanel.setLayout(new GridLayout(1,2));
			tablePanel.setBackground(labels_backgroundColor);
			
			JPanel idPanel = new JPanel();
			Label idLabel = new Label("Id:");
			idLabel.setAlignment(Label.CENTER);
			idLabel.setFont(defaultFont);
			JComboBox idCombo = new JComboBox(intValues);
			idCombo.setFont(defaultFont);
			idCombos[k] = idCombo;
			
			idCombo.setSelectedIndex(k);
			idPanel.add(idLabel);
			idPanel.add(idCombo);
			idPanel.setLayout(new GridLayout(1,2));
			idPanel.setBackground(labels_backgroundColor);
			
			JPanel panel = new JPanel();
			panel.add(idPanel);
			panel.add(tablePanel);
			panel.setLayout(new GridLayout(2,1));
			panel.setBackground(labels_backgroundColor);
			
			tempLabel[k] = new Label("TempTest");
			tempLabel[k].setAlignment(Label.CENTER);
			tempLabel[k].setFont(defaultFont);
			tempLabel[k].setBackground(backgroundColor);
			
			//combo.addActionListener(this);
			p[k].add(panel);
			p[k].add(tempLabel[k]);
		}
		for(int i = 0; i < p.length; i++) {
			if(i == 0) {
		 		fr.add( p[i], BorderLayout.WEST);
		 	} else {
		 		fr.add( p[i], BorderLayout.EAST);
		 	}
		}
		JPanel southPanel = new JPanel();
		southPanel.setBackground(backgroundColor);
		southPanel.setLayout(new BorderLayout());
		Button clearButton = new Button("Clear sent message");
		clearButton.setFont(defaultFont);
		clearButton.setBackground(labels_backgroundColor);
		clearButton.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { msgLabel.setText(""); }});
		clearButton.setPreferredSize(new Dimension(200, 30));
		msgLabel = new Label();
		msgLabel.setFont(defaultFont);
		Label sentLabel = new Label("Sent: ");
		sentLabel.setFont(defaultFont);
		southPanel.add(sentLabel, BorderLayout.WEST);
		southPanel.add(msgLabel, BorderLayout.CENTER);
		southPanel.add(clearButton, BorderLayout.EAST);
		fr.add(southPanel, BorderLayout.SOUTH);
		fr.setBackground(backgroundColor);
		fr.pack();
		fr.setVisible(true);
		return button;
	}
	
	public static void resetAllTempLabel() {
		for(int i = 0; i < tempLabel.length; i++) {
			if(tempLabel[i] != null) {
				tempLabel[i].setText("TempTest");
				tempLabel[i].setBackground(Color.WHITE);
			}
		}
	}
	
	public static void clearTempLabel(int index) {
		if(index < tempLabel.length && index >= 0) {
			if(tempLabel[index] != null) {
				tempLabel[index].setText("TempTest");
				tempLabel[index].setBackground(Color.WHITE);
			}
		}
	}
	
	public static void enableButton(int clientNumber, int buttonNumber) {
		for(int i = 0; i < buttons[clientNumber].length; i++) {
			if(i != buttonNumber) {
				buttons[clientNumber][i].setEnabled(false);
			} else {
				buttons[clientNumber][i].setEnabled(true);
				if(buttonNumber == 3) {
					buttons[clientNumber][i + 1].setEnabled(true);
					break;
				}
			}
		}
		
	}
		
	@Override  //from ActionListener
	public void actionPerformed(ActionEvent e) {
		//System.out.println("ButtonAsGui " +e );
		this.setChanged();
		this.notifyObservers(e.getActionCommand());
	}
}