%===========================================
% tearoomkb.pl
%===========================================

%% ------------------------------------------ 
%% Positions
%% ------------------------------------------ 
pos( home, 0, 0 ).
pos( barman,       5, 0 ).
pos( teatable, 1, 2, 3 ).
pos( teatable, 2, 4, 3 ).
pos( entrancedoor, 1, 4 ).
pos( exitdoor,     6, 4 ).
 
%% ------------------------------------------ 
%% N.B questi stati sono mappati anche sul CurrentState, quindi se vengono cambiati
%% vanno aggiornati anche li'
%% Teatables
	%% busy
	%% free		(not busy but not clean)
	%% dirty	(not clean)
	%% clean	(not dirty)
	%% available (free and clean)	
%% ------------------------------------------ 
%% teatable( 1, available ).
%% teatable( 2, available ).

teatable( 1, clean ).
teatable( 2, clean ).

numavailabletables(N) :-
	%%findall( N,teatable( N,available ), NList),
	findall( N,teatable( N,clean ), NList),
	%% stdout <- println( tearoomkb_numavailabletables(NList) ),
	length(NList,N).

engageTable(N,CID)	 :-
	%%stdout <- println( tearoomkb_engageTable(N) ),
	retract( teatable( N, clean ) ),
	!,
	assert( teatable( N, busy(CID) ) ).
engageTable(_,_).	

%%tableavailable(N):- teatable(N,	available ).
tableavailable(N):- teatable(N, clean ).
 

%% cleanTable(N)	 :-
%%	%% stdout <- println( tearoomkb_cleanTable(N) ),
%%	retract( teatable( N, engaged ) ),
%%	!,
%%	assert( teatable( N, clean ) ).

cleanTable(N)	 :-
	%% stdout <- println( tearoomkb_cleanTable(N) ),
	retract( teatable( N, busy(CID) ) ),
	!,
	assert( teatable( N, clean ) ).
cleanTable(N).	
 
stateOfTeatables( [teatable1(V1),teatable2(V2)] ) :-
	teatable( 1, V1 ),
	teatable( 2, V2 ).

%% ------------------------------------------ 
%% Waiter
	%%  athome
	%%	serving( CLIENTID )
	%%	movingto( cell(X,Y) )
	%%	cleaning( table(N) )
%% ------------------------------------------ 

waiter( athome ).	

%% ------------------------------------------ 
%% ServiceDesk
%% ------------------------------------------ 
%% idle
%% preparing( CLIENTID )
%% ready( CLIENTID )

servicedesk( idle ).

%% ------------------------------------------ 
%% Room as a whole
%% ------------------------------------------ 
roomstate(  waiter(S), stateOfTeatables(V), servicedesk(D) ):-
	 waiter(S), stateOfTeatables(V), servicedesk(D).
	 
	
