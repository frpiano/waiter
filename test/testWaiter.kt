package test

import org.junit.Before
import org.junit.After
import org.junit.Test
import org.junit.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
//import mapRoomKotlin.mapUtil
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.MsgUtil
import it.unibo.kactor.MqttUtils
import kotlinx.coroutines.CoroutineScope
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.CoapResponse
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.withContext
import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Default
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.Deferred
import com.fasterxml.jackson.module.kotlin.*
import domain.WaiterState
import domain.CurrentState
import domain.TableState
import com.fasterxml.jackson.databind.DeserializationFeature
import domain.Product
import domain.OrderState


class testWaiter {
	
	/*#######################################################################################################
	#											IMPORTANTE													#
	#########################################################################################################
	#	Il test necessita che siano lanciati precedentemente :												#
	#		1) basicrobot																					#
 	#		2) tearoom																						#																																															#/
	#		3) Robotweb per vedere quello che sta accadendo (opzionale)										#
	#																										#
	#	Per lanciare contemporaneamente tutti i test e' necessario fare le seguenti operazioni:				#
 	#	-	in tearoom.qak impostare la variabile jUnitWaiterTest a true in modo che non venga incrementato	#
 	#		il valore della temperatura dei clienti.														#
 	#	-	mettere a true doAllTests																		#
 	#																										#
 	#	Per lanciare i test singoli:																		#
 	#	-	impostare a false la variabile doAllTests														#
 	#	-	impostare il numero di clienti del test (da 1 a 4) in testToDo									#
 	#																										#
	#######################################################################################################*/
	/*** Indica se si vuole lanciare tutti i test oppure test singoli	***/
	val doAllTests = true
	/***	Indica quale test devono essere lanciato nel caso in cui si voglia effettuare test singoli.	***/
	val testToDo = 4
	
	var waiter: ActorBasic? = null
	var smartbell: ActorBasic? = null
	var barman: ActorBasic? = null
	var waiterwalker: ActorBasic? = null
	var simclient: ActorBasic? = null

	val mqttTest = MqttUtils("test")
	val initDelayTime = 1500L   //

	val useMqttInTest = true
	val mqttbrokerAddr = "tcp://localhost"
	val eventTopic = "unibo/polar"

	val context = "ctxwaiter"
	val destactor = "waiter"
	val addr = "127.0.0.1:8060"
	val client = CoapClient()
	val uriStr = "coap://$addr/$context/$destactor"
	val mapper = jacksonObjectMapper()
	
	var counterAtomic = java.util.concurrent.atomic.AtomicInteger()

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Before
	fun systemSetUp() {
		kotlin.concurrent.thread(start = true) {
			client.uri = uriStr

			if (useMqttInTest) {
				while (!mqttTest.connectDone()) {
					println("	attempting MQTT-conn to ${mqttbrokerAddr}  for the test unit ... ")
					Thread.sleep(1000)
					mqttTest.connect("test_nat", mqttbrokerAddr)
				}
			}
			// chiamata bloccante fare tutti i setup delle connessioni prima
			it.unibo.ctxwaiter.main()
		}
	}


	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@After
	fun terminate() {
		println("%%%  Waiter Test terminate ")
	}


	fun checkResource(value: String) {
		if (waiter != null) {
			val respGet: CoapResponse = client.get()
			val v = respGet.getResponseText()
			println("...checkResource |  $v value=$value ...")
			assertTrue(v == value)

		}
	}
	
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	@Test
	fun testWaiter() {
		runBlocking {

			while (waiter == null) {
				delay(initDelayTime)
				waiter = it.unibo.kactor.sysUtil.getActor("waiter")
			}
			println("testWaiter starts with waiter = $waiter")

			delay(5000)

			if(doAllTests){
				/***********************************************************************************************************************
	 				1 client3 che accede alla tearoom.
	 			***********************************************************************************************************************/
				var clients = arrayOf( 1, 2 )
				testWaiterOneClient()
				/***********************************************************************************************************************
	 				2 clienti che accedono alla tearoom in sequenza.
	 			***********************************************************************************************************************/
				var tables = arrayOf( 2, 1 )
				testWaiterTwoClients( clients, tables )
				/***********************************************************************************************************************
	 				3 clienti che accedono alla tearoom in sequenza. Il terzo cliente suona e prima che venga fatto entrare se ne va.
	 			***********************************************************************************************************************/
				/*
				clients = arrayOf( 1, 2, 3 )
				testWaiterMoreClients(clients , tables )
 				*/
				/***********************************************************************************************************************
	 				4 clienti che accedono alla tearoom in sequenza. Il terzo cliente suona e prima che venga fatto entrare se ne va.
	 				Il quarto e' nel corridoio mentre il secondo deve uscire. Testata la gestione dei 2 clienti nel corridio
	 			***********************************************************************************************************************/
				clients = arrayOf( 1, 2, 3, 4 )
				testWaiterMoreClients(clients , tables )
			} else {
				val tables = arrayOf( 1, 2 )
				/*	Controllo del tipo di test singolo da fare	*/
				when (testToDo) {
				    1 -> testWaiterOneClient()
				    2 -> testWaiterTwoClients( arrayOf( 1, 2 ), tables )
					3 -> testWaiterMoreClients( arrayOf( 1, 2, 3 ), tables )
				    else -> {
				        testWaiterMoreClients( arrayOf( 1, 2, 3, 4 ), tables )
				    }
				}
			}
			println("testWaiter BYE")
		}
	}

	fun checkResource(value: ActorBasic?): String {
		if (value != null) {
			return value.geResourceRep()
		} else
			return "null"
	}

	fun getWaiterCurrentState(): CurrentState {
		val resource = checkResource(waiter)
		//println(resource)
		return mapper.readValue<CurrentState>(resource);
	}
	
	/*****	Waiter State Test function	*****/
	suspend fun waiterStateTest(text : String, state : WaiterState, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => " + text + " WaiterState = " + waiterCurrState.waiterState)
		assertTrue(waiterCurrState.waiterState == state)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Table State Test function	*****/
	suspend fun tableStateTest(text : String, tableIndex : Int, state : TableState, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => " + text + " TableState = " + waiterCurrState.tableMap[tableIndex])
		assertTrue(waiterCurrState.tableMap[tableIndex] == state)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Order State Test function	*****/
	suspend fun orderStateTest(text : String, tableIndex : Int, state : OrderState, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => " + text + " TableState[$tableIndex] = " + waiterCurrState.orderMap[tableIndex])
		assertTrue(waiterCurrState.orderMap[tableIndex] == state)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Ordered Product for Table Test function	*****/
	suspend fun orderProductByTableTest(text : String, tableIndex : Int, product : Product, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => " + text + " OrderedProductByTable[$tableIndex] = " + waiterCurrState.orderedProductByTable[tableIndex])
		assertTrue(waiterCurrState.orderedProductByTable[tableIndex] == product)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Waiting client at Entrance-Door	*****/
	suspend fun waitingClientAtEntranceTest(state : Boolean, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => Testing Client is Waiting at Entrance-door... waitingClient = " + waiterCurrState.waitingClient + " ; Should be = " + state)
		assertTrue(waiterCurrState.waitingClient == state)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Waiting client at Exit-Door	*****/
	suspend fun waitingClientAtExitTest(state : Boolean, delayTime : Long){
		val waiterCurrState = getWaiterCurrentState()
		println("				**********	TEST => Testing Client is Waiting at Exit-door... waitingClientAtExit = " + waiterCurrState.waitingClientAtExit)
		assertTrue(waiterCurrState.waitingClientAtExit == state)
		println("				**********	TEST => OK")
		delay(delayTime)
	}
	
	/*****	Ending Test function	*****/
	suspend fun endingTest(ids : Array<Int>, tables : Array<Int>, totalMoney : Float){
		/*****	END CHECK	*****/
		val numberOfClients = ids.size
		var waiterCurrState = getWaiterCurrentState()
		val finalText = "				**********	FINAL TEST => "
		var index = 0
		
		println("")
		println("				############################################")
		println("					STARTING $numberOfClients CLIENT FINAL TEST")
		println("				############################################")
		println("")
		
		for(t in tables ){
			var c = 0
			if(ids.size >= 2)
				c = ids[0]
			
			println(finalText + "Testing table $t is CLEAN... TableState = " + waiterCurrState.tableMap[t])
			assertTrue(waiterCurrState.tableMap[t] == TableState.CLEAN)
			println(finalText + "OK")
			println(finalText + "Testing order of client $c at table $t is Nothing... OrderedProductByTable[$t] = " + waiterCurrState.orderedProductByTable[t])
			assertTrue(waiterCurrState.orderedProductByTable[t] == Product.NONE)
			println(finalText + "OK")
			println(finalText + "Testing order state of client $c at table $t is COMPLETED... TableState[$t] = " + waiterCurrState.orderMap[t])
			assertTrue(waiterCurrState.orderMap[t] == OrderState.COMPLETE)
			println(finalText +"OK")
			index++
		}
		waiterCurrState = getWaiterCurrentState()
		println(finalText + "Testing TOTAL EARNED MONEY... moneyEarned = " + waiterCurrState.moneyEarned + "€")
		assertTrue(waiterCurrState.moneyEarned == totalMoney)
		println("")
		println("				############################################")
		println("				$numberOfClients CLIENT TEST COMPLETED CORRECTLY! GOOD JOB!")
		println("				############################################")
		println("")
	}
	
	/********************************************************************************/
	/*								TEST 1 CLIENT									*/
	/********************************************************************************/
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun testWaiterOneClient(){
		
		val client = 1
		val clientTables = arrayOf( 1, 2 )
		var table = clientTables[0]
		
		println(" --- test Waiter One Clients ---")

		if (waiter != null) {
			delay(150)
			var moneyEarned : Float = 0.0f
			var testIndex = 1
			
			val startingMoneyEarned = getWaiterCurrentState().moneyEarned
			
			waiterStateTest("Testing waiter is AT HOME... WaiterState = ", WaiterState.AT_HOME, 200)

			/*****	cliente 1 notifica la volontà di entrare e viene fatto entrare	*****/
			sendNotifyEntrance(client)
			delay(1000)
			
			var tempState = WaiterState.AT_HOME
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 1	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED ),
				arrayOf(
					"",
					"Testing waiter MOVING TO ENTRANCE...",
					"Testing waiter is AT ENTRANCE...",
					"Testing waiter is DEPLOYING client $client to table $table...",
					"Testing waiter has DEPLOYED CLIENT $client to table $table..."
				)
			)
			tempState = WaiterState.CLIENT_DEPLOYED
			testIndex++
			
			tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 2	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("", "Testing waiter is MOVING TO HOME...")
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++

			/*****	Cliente 1 vuole ordinare ed ordina TEA	*****/
			sendReadyForOrder(client, table)
			delay(2000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 3	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER ),
				arrayOf("", "Testing waiter is READY FOR ORDER of table $client...", "Testing waiter is GETTING ORDER of table $client...")
			)
			tempState = WaiterState.GET_ORDER
			testIndex++
			
			var clientOrderName = Product.TEA.name

			sendClientOrder(clientOrderName, client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 4	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf(tempState, WaiterState.TRANSMITTING_ORDER),
				arrayOf("", "Testing waiter is TRANSMITTING order...")
			)
			tempState = WaiterState.TRANSMITTING_ORDER
			testIndex++
			/****************************************************/
			
			/*****	Waiter va a prendere l'ordine del cliente 1 dal barman e lo serve	*****/
			clientOrderName = Product.TEA.name
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 5	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME, WaiterState.MOVING_TO_BARMAN, WaiterState.DELIVER_ORDER, WaiterState.SERVING),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is MOVING TO BARMAN...",
						"Testing waiter is DELIVERING order to client $client at table $table...",
						"Testing waiter is SERVING order to client $client at table $table..."
				)
			)
			tempState = WaiterState.SERVING
			testIndex++
			
			orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.TEA, 0)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 6	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME..."
				)
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++
			
			/*****	Cliente 1 termina di consumare, paga e viene portato all'uscita	*****/
			
			sendConsumeFinished(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 7	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is VERIFYING ORDER for client $client at table $table...",
						"Testing waiter is COLLECTING payment from client $client at table $table..."
				)
			)
			tempState = WaiterState.COLLECTING
			testIndex++
			
			sendPayment(client, Product.TEA.price)
			moneyEarned += Product.TEA.price
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 8	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
				arrayOf("",
						"Testing waiter has COLLECTED PAYMENT from client $client at table $table...",
						"Testing waiter is MOVING_TO_EXIT to convey out client $client from table $table...",
						"Testing waiter is AT EXIT..."
				)
			)
			tempState = WaiterState.AT_EXIT
			testIndex++
			/****************************************************/
			
			/*****	Pulizia Tavolo 1	*****/
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 3000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 9	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING, WaiterState.CLEANED),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table...",
						"Testing waiter has CLEANED table $table..."
				)
			)
			tempState = WaiterState.CLEANED
			testIndex++
			
			tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 100)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 10	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME, WaiterState.AT_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is AT HOME..."
				)
			)
			//tempState = WaiterState.AT_HOME
			//testIndex++
			
			/*****	Ending Test	*****/
			endingTest(arrayOf( client ), clientTables, startingMoneyEarned + moneyEarned)
			/****************************************************/
		} else {
			println("Waiter is null.....")
		}
		assert(true)
	}
	
	/********************************************************************************/
	/*								TEST 2 CLIENTS									*/
	/********************************************************************************/
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun testWaiterTwoClients(clientIds: Array<Int>, clientTables: Array<Int>) {
		println(" --- test Waiter Two Clients ---")

		if (waiter != null) {
			delay(150)
			var moneyEarned : Float = 0.0f
			var testIndex = 1
			
			val startingMoneyEarned = getWaiterCurrentState().moneyEarned
			
			waiterStateTest("Testing waiter is AT HOME... WaiterState = ", WaiterState.AT_HOME, 200)

			/*****	cliente 1 notifica la volontà di entrare e viene fatto entrare	*****/
			sendNotifyEntrance(clientIds[0])
			delay(1000)
			waitingClientAtEntranceTest(false, 1000)
			
			var table = clientTables[0]
			var client = clientIds[0]
			
			var tempState = WaiterState.AT_HOME
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 1	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED ),
				arrayOf(
					"",
					"Testing waiter MOVING TO ENTRANCE...",
					"Testing waiter is AT ENTRANCE...",
					"Testing waiter is DEPLOYING client $client to table $table...",
					"Testing waiter has DEPLOYED CLIENT $client to table $table..."
				)
			)
			tempState = WaiterState.CLIENT_DEPLOYED
			testIndex++
			
			tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			/****************************************************/
			
			/*****	cliente 2 notifica la volontà di entrare e viene fatto entrare	*****/
			sendNotifyEntrance(clientIds[1])
			delay(1000)
			waitingClientAtEntranceTest(false, 1000)
			
			table = clientTables[1]
			client = clientIds[1]
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 2	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED ),
				arrayOf(
					"",
					"Testing waiter MOVING TO ENTRANCE...",
					"Testing waiter is AT ENTRANCE...",
					"Testing waiter DEPLOYING client $client to table $table...",
					"Testing waiter has DEPLOYED CLIENT $client to table $table..."
				)
			)
			tempState = WaiterState.CLIENT_DEPLOYED
			testIndex++
			
			tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 3	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("", "Testing waiter is MOVING TO HOME...")
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++

			/*****	Cliente 1 vuole ordinare ed ordina TEA	*****/
			table = clientTables[0]
			client = clientIds[0]

			sendReadyForOrder(client, table)
			delay(2000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 4	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER ),
				arrayOf("", "Testing waiter is READY FOR ORDER of table $client...", "Testing waiter is GETTING ORDER of table $client...")
			)
			tempState = WaiterState.GET_ORDER
			testIndex++
			
			var clientOrderName = Product.TEA.name

			sendClientOrder(clientOrderName, client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 5	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf(tempState, WaiterState.TRANSMITTING_ORDER),
				arrayOf("", "Testing waiter is TRANSMITTING order...")
			)
			tempState = WaiterState.TRANSMITTING_ORDER
			testIndex++
			
			/****************************************************/
			
			/*****	Cliente 2 vuole ordinare ed ordina COKE	*****/
			table = clientTables[1]
			client = clientIds[1]

			sendReadyForOrder(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 6	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER),
				arrayOf("", "Testing waiter is READY FOR ORDER of table $client...", "Testing waiter is GETTING ORDER of table $client...")
			)
			tempState = WaiterState.GET_ORDER
			testIndex++
			
			clientOrderName = Product.COKE.name

			sendClientOrder(clientOrderName, client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 7	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TRANSMITTING_ORDER),
				arrayOf("", "Testing waiter is TRANSMITTING order...")
			)
			tempState = WaiterState.TRANSMITTING_ORDER
			testIndex++
			
			/****************************************************/
			
			/*****	Waiter va a prendere l'ordine del cliente 1 dal barman e lo serve	*****/
			client = clientIds[0]
			table = clientTables[0]
			clientOrderName = Product.TEA.name
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 8	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME ,WaiterState.MOVING_TO_BARMAN, WaiterState.DELIVER_ORDER, WaiterState.SERVING),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is MOVING TO BARMAN...",
						"Testing waiter is DELIVERING order to client $client at table $table...",
						"Testing waiter is SERVING order to client $client at table $table..."
				)
			)
			tempState = WaiterState.SERVING
			testIndex++
			
			orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.TEA, 0)
			/****************************************************/
			
			/*****	Waiter va a prendere l'ordine del cliente 2 dal barman e lo serve	*****/
			client = clientIds[1]
			table = clientTables[1]
			clientOrderName = Product.COKE.name
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 9	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_BARMAN ,WaiterState.DELIVER_ORDER, WaiterState.SERVING),
				arrayOf("",
						"Testing waiter is MOVING TO BARMAN...",
						"Testing waiter is DELIVERING order to client $client at table $table...",
						"Testing waiter is SERVING order to client $client at table $table..."
				)
			)
			tempState = WaiterState.SERVING
			testIndex++
			
			orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.COKE, 0)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 10	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME..."
				)
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++
			
			
			/*****	Cliente 1 termina di consumare, paga e viene portato all'uscita	*****/
			client = clientIds[0]
			table = clientTables[0]
			
			sendConsumeFinished(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 11	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is VERIFYING ORDER for client $client at table $table...",
						"Testing waiter is COLLECTING payment from client $client at table $table..."
				)
			)
			tempState = WaiterState.COLLECTING
			testIndex++
			
			sendPayment(client, Product.TEA.price)
			moneyEarned += Product.TEA.price
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 12	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
				arrayOf("",
						"Testing waiter has COLLECTED PAYMENT from client $client at table $table...",
						"Testing waiter is MOVING_TO_EXIT to convey out client $client from table $table...",
						"Testing waiter is AT EXIT..."
				)
			)
			tempState = WaiterState.AT_EXIT
			testIndex++
			
			/****************************************************/
						
			/*****	Pulizia Tavolo 1 ed interruzione da parte del cliente 2	*****/
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 0)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 13	##########*/
			/*####################################################*/
			
			// Waiter pulisce tavolo ma viene interrotto da cliente al tavolo 2 che vuole uscire
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table..."
				)
			)
			tempState = WaiterState.CLEANING
			testIndex++
			
			/****************************************************/
			
			/**** Cliente 2 termina di consumare, paga e viene portato all'uscita	*****/
			client = clientIds[1]
			table = clientTables[1]
			sendConsumeFinished(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 14	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is VERIFYING ORDER for client $client at table $table...",
						"Testing waiter is going to COLLECT payment from client $client at table $table..."
				)
			)
			tempState = WaiterState.COLLECTING
			testIndex++
			
			sendPayment(client, Product.COKE.price)
			moneyEarned += Product.COKE.price
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 15	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is going to COLLECT payment from client $client at table $table...",
						"Testing waiter is AT EXIT..."
				)
			)
			tempState = WaiterState.AT_EXIT
			testIndex++
			/****************************************************/
			
			/*****	Pulizia Tavolo 1 poiche' ha tempo minore per completare la pulizia	*****/
			client = clientIds[0]
			table = clientTables[0]
			
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 3000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 16	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING, WaiterState.CLEANED),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table...",
						"Testing waiter has CLEANED table $table..."
				)
			)
			tempState = WaiterState.CLEANED
			testIndex++
			
			tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 100)
			
			/****************************************************/
			
			/*****	Pulizia Tavolo 2	*****/
			client = clientIds[1]
			table = clientTables[1]
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 17	##########*/
			/*####################################################*/
			
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 0)
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING, WaiterState.CLEANED),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table....",
						"Testing waiter has CLEANED table $table...."
				)
			)
			tempState = WaiterState.CLEANED
			testIndex++
			
			tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 0)
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 18	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME, WaiterState.AT_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is AT HOME..."
				)
			)
			//tempState = WaiterState.AT_HOME
			//testIndex++
			
			/*****	Ending Test	*****/
			endingTest(clientIds, clientTables, startingMoneyEarned + moneyEarned)
			/****************************************************/
		} else {
			println("Waiter is null.....")
		}
		assert(true)
	}
	
	/********************************************************************************/
	/*							TEST 3 OR MORE CLIENTS								*/
	/********************************************************************************/
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun testWaiterMoreClients(clientIds: Array<Int>, clientTables: Array<Int>) {
		val clientsNumber = clientIds.size
		println(" --- test Waiter $clientsNumber Clients ---")

		if (waiter != null) {
			delay(150)
			var moneyEarned : Float = 0.0f
			var testIndex = 1
			
			val startingMoneyEarned = getWaiterCurrentState().moneyEarned
			
			waiterStateTest("Testing waiter is AT HOME... WaiterState = ", WaiterState.AT_HOME, 200)

			/*****	cliente 1 notifica la volontà di entrare e viene fatto entrare	*****/
			sendNotifyEntrance(clientIds[0])
			delay(1000)
			waitingClientAtEntranceTest(false, 1000)
			
			var table = clientTables[0]
			var client = clientIds[0]
			var tempState = WaiterState.AT_HOME
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 1	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED ),
				arrayOf(
					"",
					"Testing waiter MOVING TO ENTRANCE...",
					"Testing waiter is AT ENTRANCE...",
					"Testing waiter is DEPLOYING client $client to table $table...",
					"Testing waiter has DEPLOYED CLIENT $client to table $table..."
				)
			)
			tempState = WaiterState.CLIENT_DEPLOYED
			testIndex++
			tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			/****************************************************/
			
			/*****	cliente 2 notifica la volontà di entrare e viene fatto entrare	*****/
			sendNotifyEntrance(clientIds[1])
			delay(1000)
			waitingClientAtEntranceTest(false, 1000)
			
			table = clientTables[1]
			client = clientIds[1]
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 2	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED ),
				arrayOf(
					"",
					"Testing waiter MOVING TO ENTRANCE...",
					"Testing waiter is AT ENTRANCE...",
					"Testing waiter DEPLOYING client $client to table $table...",
					"Testing waiter has DEPLOYED CLIENT $client to table $table..."
				)
			)
			tempState = WaiterState.CLIENT_DEPLOYED
			testIndex++
			tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			/****************************************************/
			
			/*****	cliente 3 vuole entrare ma deve aspettare che si liberi un posto	*****/
			if(clientIds.size > 2){
				sendNotifyEntrance(clientIds[2])
				delay(1000)
				waitingClientAtEntranceTest(true, 1000)
			}
			/****************************************************/
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 3	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("", "Testing waiter is MOVING TO HOME...")
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++

			/*****	Cliente 1 vuole ordinare ed ordina TEA	*****/
			table = clientTables[0]
			client = clientIds[0]

			sendReadyForOrder(client, table)
			delay(2000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 4	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER ),
				arrayOf("", "Testing waiter is READY FOR ORDER of table $client...", "Testing waiter is GETTING ORDER of table $client...")
			)
			tempState = WaiterState.GET_ORDER
			testIndex++
			
			var clientOrderName = Product.TEA.name
			sendClientOrder(clientOrderName, client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 5	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf(tempState, WaiterState.TRANSMITTING_ORDER),
				arrayOf("", "Testing waiter is TRANSMITTING order...")
			)
			tempState = WaiterState.TRANSMITTING_ORDER
			testIndex++
			/****************************************************/
			
			/*****	Cliente 2 vuole ordinare ed ordina COKE	*****/
			table = clientTables[1]
			client = clientIds[1]

			sendReadyForOrder(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 6	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER),
				arrayOf("", "Testing waiter is READY FOR ORDER of table $client...", "Testing waiter is GETTING ORDER of table $client...")
			)
			tempState = WaiterState.GET_ORDER
			testIndex++
			clientOrderName = Product.COKE.name
			sendClientOrder(clientOrderName, client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 7	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TRANSMITTING_ORDER),
				arrayOf("", "Testing waiter is TRANSMITTING order...")
			)
			tempState = WaiterState.TRANSMITTING_ORDER
			testIndex++
			/****************************************************/
			
			/*****	Waiter va a prendere l'ordine del cliente 1 dal barman e lo serve	*****/
			client = clientIds[0]
			table = clientTables[0]
			clientOrderName = Product.TEA.name
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 8	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME ,WaiterState.MOVING_TO_BARMAN, WaiterState.DELIVER_ORDER, WaiterState.SERVING),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is MOVING TO BARMAN...",
						"Testing waiter is DELIVERING order to client $client at table $table...",
						"Testing waiter is SERVING order to client $client at table $table..."
				)
			)
			tempState = WaiterState.SERVING
			testIndex++
			orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.TEA, 0)
			/****************************************************/
			
			/*****	Waiter va a prendere l'ordine del cliente 2 dal barman e lo serve	*****/
			client = clientIds[1]
			table = clientTables[1]
			clientOrderName = Product.COKE.name
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 9	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_BARMAN ,WaiterState.DELIVER_ORDER, WaiterState.SERVING),
				arrayOf("",
						"Testing waiter is MOVING TO BARMAN...",
						"Testing waiter is DELIVERING order to client $client at table $table...",
						"Testing waiter is SERVING order to client $client at table $table..."
				)
			)
			tempState = WaiterState.SERVING
			testIndex++
			orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.COKE, 0)
			/****************************************************/
						
			/*####################################################*/
			/*##########	Waiter States Flow Test 10	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME..."
				)
			)
			tempState = WaiterState.MOVING_TO_HOME
			testIndex++
			
			/***** Cliente 3 non aspetta e se ne va	*****/
			if(clientIds.size > 2){
				waitingClientAtEntranceTest(true, 0)
				emitClientGone()
				delay(1000)
				waitingClientAtEntranceTest(false, 200)
			}
			
			/*****	Cliente 1 termina di consumare, paga e viene portato all'uscita	*****/
			client = clientIds[0]
			table = clientTables[0]
			
			sendConsumeFinished(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 11	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is VERIFYING ORDER for client $client at table $table...",
						"Testing waiter is COLLECTING payment from client $client at table $table..."
				)
			)
			tempState = WaiterState.COLLECTING
			testIndex++
			
			sendPayment(client, Product.TEA.price)
			moneyEarned += Product.TEA.price
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 12	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
				arrayOf("",
						"Testing waiter has COLLECTED PAYMENT from client $client at table $table...",
						"Testing waiter is MOVING_TO_EXIT to convey out client $client from table $table...",
						"Testing waiter is AT EXIT..."
				)
			)
			tempState = WaiterState.AT_EXIT
			testIndex++
			/****************************************************/
						
			/*****	Pulizia Tavolo 1 ed interruzione da parte del cliente 2	*****/
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 0)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 13	##########*/
			/*####################################################*/
			// Waiter pulisce tavolo ma viene interrotto da cliente al tavolo 2 che vuole uscire
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table..."
				)
			)
			tempState = WaiterState.CLEANING
			testIndex++
			/****************************************************/
			
			/**** Cliente 2 termina di consumare, paga e viene portato all'uscita	*****/
			client = clientIds[1]
			table = clientTables[1]
			sendConsumeFinished(client, table)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 14	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is VERIFYING ORDER for client $client at table $table...",
						"Testing waiter is going to COLLECT payment from client $client at table $table..."
				)
			)
			tempState = WaiterState.COLLECTING
			testIndex++
			
			sendPayment(client, Product.COKE.price)
			moneyEarned += Product.COKE.price
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 15	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
				arrayOf("",
						"Testing waiter is going to TAKE OUT client $client at table $table...",
						"Testing waiter is going to COLLECT payment from client $client at table $table...",
						"Testing waiter is AT EXIT..."
				)
			)
			tempState = WaiterState.AT_EXIT
			testIndex++
			/*****************************************************/
			/*****	cliente 4 vuole entrare	*****/
			if(clientIds.size > 3){
				sendNotifyEntrance(clientIds[3])
				delay(1000)
				waitingClientAtEntranceTest(true, 200)
			}
			/****************************************************/
			
			/*****	Pulizia Tavolo 1 poiche' ha tempo minore per completare la pulizia	*****/
			client = clientIds[0]
			table = clientTables[0]
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 3000)
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 16	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING, WaiterState.CLEANED),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table...",
						"Testing waiter has CLEANED table $table..."
				)
			)
			tempState = WaiterState.CLEANED
			testIndex++
			tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 100)
			/****************************************************/
			
			/*****	Cliente 4 viene fatto entrare e portato al tavolo 1	****/
			if(clientIds.size > 3){
				client = clientIds[3]
				table = clientTables[0]
				/*####################################################*/
				/*##########	Waiter States Flow Test 17	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.MOVING_TO_ENTRANCE, WaiterState.AT_ENTRANCE, WaiterState.DEPLOY_CLIENT, WaiterState.CLIENT_DEPLOYED),
					arrayOf("",
							"Testing waiter is MOVING TO ENTRANCE...",
							"Testing waiter is AT ENTRANCE...",
							"Testing waiter DEPLOYING client $client to table $table...",
							"Testing waiter has DEPLOYED CLIENT $client to table $table..."
					)
				)
				tempState = WaiterState.CLIENT_DEPLOYED
				testIndex++
				tableStateTest("Testing table $table is BUSY...", table, TableState.BUSY, 0)
			}
			/****************************************************/
			testIndex = 17
			
			/*****	Pulizia Tavolo 2	*****/
			client = clientIds[1]
			table = clientTables[1]
			
			/*####################################################*/
			/*##########	Waiter States Flow Test 18	##########*/
			/*####################################################*/
			tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 0)
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING, WaiterState.CLEANED),
				arrayOf("",
						"Testing waiter is GOING TO CLEAN table $table...",
						"Testing waiter is CLEANING table $table....",
						"Testing waiter has CLEANED table $table...."
				)
			)
			tempState = WaiterState.CLEANED
			testIndex++
			tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 0)
			/****************************************************/
			
			/*****	Cliente 4 ordina ed attende che scada maxStayTime. Successivamente paga e viene portato all'uscita	****/
			if(clientIds.size > 3){
				table = clientTables[0]
				client = clientIds[3]
				
				/*****	Cliente 4 vuole ordinare ed ordina TEA	*****/
				sendReadyForOrder(client, table)
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 19	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.READY_FOR_ORDER, WaiterState.GET_ORDER),
					arrayOf("",
							"Testing waiter is READY FOR ORDER for client $client at table $table...",
							"Testing waiter is GETTING ORDER of table $client..."
					)
				)
				tempState = WaiterState.GET_ORDER
				testIndex++
				
				clientOrderName = Product.GINTONIC.name
				sendClientOrder(Product.GINTONIC.name, client, table)
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 20	##########*/
				/*####################################################*/
				/*****	Waiter va a prendere l'ordine del cliente 4 dal barman e lo serve	*****/
				flowWaiterStateTesting(
					testIndex,
					arrayOf(	tempState,
								WaiterState.TRANSMITTING_ORDER,
								WaiterState.MOVING_TO_HOME,
								WaiterState.MOVING_TO_BARMAN,
								WaiterState.DELIVER_ORDER,
								WaiterState.SERVING
					),
					arrayOf("",
							"Testing waiter is TRANSMITTING order $clientOrderName to barman...",
							"Testing waiter is MOVING TO HOME...",
							"Testing waiter is MOVING TO BARMAN...",
							"Testing waiter is DELIVERING ORDER $clientOrderName to client $client at table $client...",
							"Testing waiter is SERVING order $clientOrderName to client $client at table $table..."
					)
				)
				tempState = WaiterState.SERVING
				testIndex++
				
				var timeToGoHome : Long = System.currentTimeMillis();
				/****************************************************/
				
				orderProductByTableTest("Testing order $clientOrderName of client $client is served at table $table...", table, Product.GINTONIC, 0)
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 21	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.MOVING_TO_HOME, WaiterState.AT_HOME),
					arrayOf("",
							"Testing waiter is MOVING TO HOMR...",
							"Testing waiter is AT HOME..."
					)
				)
				tempState = WaiterState.AT_HOME
				testIndex++
				/*****	Client 4 aspetta che scada maxStayTime, paga quando il waiter arriva al tavolo e viene portato all'uscita	*****/
				timeToGoHome = System.currentTimeMillis() - timeToGoHome
				val maxStayTime = getWaiterCurrentState().maxStayTime
				delay(maxStayTime - timeToGoHome + 100)
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 22	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.TAKE_OUT, WaiterState.VERIFY_ORDER, WaiterState.COLLECTING),
					arrayOf("",
							"Testing waiter is going to TAKE OUT client $client at table $table...",
							"Testing waiter is VERIFYING ORDER for client $client at table $table...",
							"Testing waiter is COLLECTING payment from client $client at table $table..."
					)
				)
				tempState = WaiterState.COLLECTING
				testIndex++
								
				sendPayment(client, Product.GINTONIC.price)
				moneyEarned += Product.GINTONIC.price
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 23	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.PAYMENT_COLLECTED, WaiterState.MOVING_TO_EXIT, WaiterState.AT_EXIT),
					arrayOf("",
							"Testing waiter has COLLECTED PAYMENT from client $client at table $table...",
							"Testing waiter is MOVING TO EXIT to convey out client $client from table $table...",
							"Testing waiter is AT EXIT..."
					)
				)
				tempState = WaiterState.AT_EXIT
				testIndex++
				/****************************************************/
							
				/*****	Pulizia Tavolo 1	*****/
				tableStateTest("Testing table $table is DIRTY...", table, TableState.DIRTY, 0)
				
				/*####################################################*/
				/*##########	Waiter States Flow Test 24	##########*/
				/*####################################################*/
				flowWaiterStateTesting(
					testIndex,
					arrayOf( tempState, WaiterState.GOING_CLEAN, WaiterState.CLEANING,  WaiterState.CLEANED),
					arrayOf("",
							"Testing waiter is GOING TO CLEAN table $table...",
							"Testing waiter is CLEANING table $table...",
							"Testing waiter is AT EXIT..."
					)
				)
				tempState = WaiterState.CLEANED
				testIndex++
				tableStateTest("Testing table $table is CLEAN...", table, TableState.CLEAN, 0)
				/****************************************************/
			}
			/****************************************************/
			testIndex = 25
			/*####################################################*/
			/*##########	Waiter States Flow Test 25	##########*/
			/*####################################################*/
			flowWaiterStateTesting(
				testIndex,
				arrayOf( tempState, WaiterState.MOVING_TO_HOME, WaiterState.AT_HOME),
				arrayOf("",
						"Testing waiter is MOVING TO HOME...",
						"Testing waiter is AT HOME..."
				)
			)
			//tempState = WaiterState.AT_HOME
			//testIndex++
			
			/*****	Ending Test	*****/
			endingTest(clientIds, clientTables, startingMoneyEarned + moneyEarned)
			/****************************************************/	
		} else {
			println("Waiter is null.....")
		}
		assert(true)
	}

	suspend fun flowWaiterStateTesting(testId : Int, states : Array<WaiterState>, messageString : Array<String>){
		val shouldDisplayMultipleTimes = false
		var messageDisplayed = false
		var index = 1
		var shouldProceed = false
		while(!shouldProceed){
			val waiterCurrState = getWaiterCurrentState()
			//println("		**********	TEST $testId => " + states[index] + " WaiterState = " + waiterCurrState.waiterState + " ; Should be => " + states[index])
			if(waiterCurrState.waiterState == states[index]){
				assertTrue(waiterCurrState.waiterState == states[index])
				println("		**********	TEST $testId => State '" + states[index] + " : OK")
				index++
				if(index >= states.size ){
					shouldProceed = true
				}
				messageDisplayed = false
			} else if(waiterCurrState.waiterState == states[index - 1]){
				//println("		**********	TEST => DELAYING TEST! " + messageString[index] + " WaiterState = " + waiterCurrState.waiterState + " ; Should be => " + states[index])
				if(shouldDisplayMultipleTimes || !messageDisplayed){
					println("		**********	TEST $testId => DELAYING TEST! " + states[index] + " WaiterState = " + waiterCurrState.waiterState + " ; Should be => " + states[index])
					messageDisplayed = true
				}
			} else {
				println("		**********	TEST $testId => " + states[index] + " WaiterState = " + waiterCurrState.waiterState + " ; Should be => " + states[index])
				println("		**********	TEST $testId => " + messageString[index] + " FAILS")
				shouldProceed = true
				assertTrue(waiterCurrState.waiterState == states[index])
				messageDisplayed = false
			}
			delay(100)				
		}
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun sendAskForTable(id : Int) {
		//-----------------TEST READY FOR ORDER--------------
		println("---------------------------	SENDING ASK FOR TABLE	: ID = $id	---------------------------")

		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "askForTable", "askForTable(0)", "waiter"), mqttTest)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "askForTable", "askForTable(0)", "waiter"),
				waiter!!
			)
		}
		delay(100)
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun sendReadyForOrder(id : Int, table : Int) {
		//-----------------TEST READY FOR ORDER--------------
		println("---------------------------	SENDING READY FOR ORDER	: ID = $id ; TABLE = $table	---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "readyForOrder", "readyForOrder($id,$table)", "waiter"),
				mqttTest
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "readyForOrder", "readyForOrder($id,$table)", "waiter"),
				waiter!!
			)
		}
		delay(100)
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun sendClientOrder(OrderName : String, id : Int, table : Int) {
		//-----------------TEST CLIENT ORDER--------------
		println("---------------------------	SENDING CLIENT ORDER : NAME = $OrderName ; ID = $id ; TABLE = $table		---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildReply("test" + id, "clientOrder", "clientOrder($OrderName,$id,$table)", "waiter"),
				mqttTest
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildReply("test" + id, "clientOrder", "clientOrder($OrderName,$id,$table)", "waiter"),
				waiter!!
			)
		}
		delay(100)
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	suspend fun sendConsumeFinished(id : Int, table : Int) {
		//-----------------TEST CLIENT CONSUME FINISHED--------------
		println("---------------------------	SENDING CONSUME FINISHED : ID = $id ; TABLE = $table		---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "consumeFinished", "consumeFinished($id,$table)", "waiter"),
				mqttTest
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildDispatch("test" + id, "consumeFinished", "consumeFinished($id,$table)", "waiter"),
				waiter!!
			)
		}
		delay(100)
	}

	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun sendPayment(id : Int, moneyValue : Float) {
		//-----------------TEST CLIENT PAYMENT--------------
		println("---------------------------	SENDING PAYMENT : ID = $id ; MONEY = $moneyValue		---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildReply("test" + id, "payment", "payment($moneyValue)", "waiter"),
				mqttTest
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildReply("test" + id, "payment", "payment($moneyValue)", "waiter"),
				waiter!!
			)
		}
		delay(100)
	}
	
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun emitClientGone() {
		//-----------------TEST CLIENT GONE EVENT--------------
		println("---------------------------	EMITTING CLIENT GONE EVENT		---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildEvent("testClientGone", "clientGone", "clientGone(0)"),
				mqttTest, "unibo/polar"
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildEvent("testClientGone", "clientGone", "clientGone(0)"),
				waiter!!
			)
		}
		delay(100)
	}
	
	@kotlinx.coroutines.ObsoleteCoroutinesApi
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	suspend fun sendNotifyEntrance( id : Int ){
		//-----------------TEST CLIENT GONE EVENT--------------
		println("---------------------------	SENDING NOTIFY ENTRANCE		---------------------------")
		if (useMqttInTest) {
			MsgUtil.sendMsg(
				MsgUtil.buildRequest("testNotifyEntrance" + id, "notifyEntrance", "notifyEntrance(0)", "smartbell"),
				mqttTest, "unibo/polar"
			)
		} else {
			MsgUtil.sendMsg(
				MsgUtil.buildRequest("testNotifyEntrance" + id, "notifyEntrance", "notifyEntrance(0)", "smartbell"),
				smartbell!!
			)
		}
		delay(100)
	}
	
}
