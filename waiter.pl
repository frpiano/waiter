%====================================================================================
% waiter description   
%====================================================================================
mqttBroker("localhost", "1883", "unibo/polar").
context(ctxwaiter, "localhost",  "TCP", "8060").
context(ctxtearoom, "127.0.0.1",  "TCP", "8010").
 qactor( waiterwalker, ctxwaiter, "it.unibo.waiterwalker.Waiterwalker").
  qactor( barman, ctxtearoom, "external").
  qactor( smartbell, ctxtearoom, "external").
  qactor( clientsimulator, ctxtearoom, "external").
  qactor( waiter, ctxwaiter, "it.unibo.waiter.Waiter").
